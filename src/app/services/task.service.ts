import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import Item from '../Item';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  private api_url = 'http://localhost:5000/all_items';

  constructor(private http: HttpClient) {}

  get_items(): Observable<Item[]> {
    return this.http.get<Item[]>(this.api_url);
  }

  delete_item(item: Item): Observable<Item> {
    const url = `${this.api_url}/${item.id}`;
    return this.http.delete<Item>(url);
  }

  update_item(item: Item): Observable<Item> {
    const url = `${this.api_url}/${item.id}`;
    return this.http.put<Item>(url, item, httpOptions);
  }

  add_item(item: Item): Observable<Item> {
    return this.http.post<Item>(this.api_url, item, httpOptions);
  }
}
