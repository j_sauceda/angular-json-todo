import { Component, Input, Output, EventEmitter } from '@angular/core';
import Item from 'src/app/Item';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'],
})
export class ItemComponent {
  @Input() item!: Item;
  @Output() update = new EventEmitter<Item>();
  @Output() remove = new EventEmitter<Item>();
  editable = false;
}
