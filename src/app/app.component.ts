import { Component, OnInit } from '@angular/core';

import { TaskService } from './services/task.service';
import { Filter } from './Filter';
import Item from './Item';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'json-server TODO app';
  appFilter = Filter;

  current_filter = this.appFilter.all;

  all_tasks: Item[] = [];

  constructor(private taskService: TaskService) {}

  ngOnInit(): void {
    this.get_tasks();
  }

  get_tasks() {
    this.taskService.get_items().subscribe((all_items) => {
      this.all_tasks = all_items;
    });
  }

  get filter_tasks() {
    // debugger; // debugging example for teaching purposes
    if (this.current_filter === this.appFilter.all) {
      return this.all_tasks;
    }
    if (this.current_filter === this.appFilter.done) {
      return this.all_tasks.filter((item) => item.done);
    }
    return this.all_tasks.filter((item) => !item.done);
  }

  delete_task(task: Item) {
    this.taskService.delete_item(task).subscribe(() => {
      this.all_tasks = this.all_tasks.filter((t) => t.id !== task.id);
    });
  }

  update_task(task: Item) {
    this.taskService.update_item(task).subscribe(() => {
      this.get_tasks();
    });
  }

  add_task(description: string) {
    if (!description) return;
    let task: Item = { description, done: false };
    this.taskService.add_item(task).subscribe((new_task) => {
      this.all_tasks.push(new_task);
    });
  }
}
