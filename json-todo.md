# Fullstack TODO app - con json-server

## Backend setup

- $ ng new json-todo
- $ cd json-todo
- $ npm install json-server
- editar package.json:

```json
{
  "name": "json-todo",
  "version": "0.0.0",
  "scripts": {
    "ng": "ng",
    "start": "ng serve",
    "build": "ng build",
    "watch": "ng build --watch --configuration development",
    "test": "ng test",
    "backend": "json-server --watch db.json --port 5000"
  },
}
```

- crear db.json en /json-todo: $ touch db.json
- editar db.json:

```json
{
    "all_items": [
        {
            "id": 1,
            "description": "Despertar",
            "done": true
        },
        {
            "id": 2,
            "description": "Tomar una ducha",
            "done": true
        },
        {
            "id": 3,
            "description": "Cambiarse",
            "done": true
        },
        {
            "id": 4,
            "description": "Desayunar",
            "done": true
        },
        {
            "id": 5,
            "description": "Salir a trabajar",
            "done": true
        }
    ]
}
```

- probar: $ npm run backend
- navegar en http://localhost:5000
- navegar en http://localhost:5000/all_items
- si funciona, las operaciones CRUD están habilitadas:

## Frontend setup

- $ cd AngularApp
- instalación de Bootstrap basada en [este artículo en TechDiaries](https://www.techiediaries.com/angular-bootstrap/)
- NO FUNCIONA: $ ng add @ng-bootstrap/schematics
- alternativa a $ ng add ... => $ npm install bootstrap jquery
- $ cd src
- editar styles.css:

```css
@import "~bootstrap/dist/css/bootstrap.css"
```

- editar angular.json en /AngularApp:

```JS
{
	...
    "projects": {
    	"architect": {
        	"build": {
            	"options": {
                	"scripts": [
                      "./node_modules/jquery/dist/jquery.js",
                      "./node_modules/bootstrap/dist/js/bootstrap.js"
                    ]
                }
            }
        }
    }
}
```

- editar index.html:

```xml
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>json-TODO app</title>
  <base href="/">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
</head>
<body>
  <app-root></app-root>
</body>
</html>

```

- $ cd app
- edit app.component.ts:

```javascript
...
title = 'json-server TODO app';
...
```

- edit app.component.html:

```xml
<div class="container">
  <h1>
    {{ title }}
  </h1>
  
  What would you like to do today?

  <hr class="border border-primary border-3 opacity-75" />

  TODO app aquí
</div>
```

- $ ng serve
- $ touch Item.ts
- editar Item.ts:

```javascript
export default interface Item {
  id?: number,
  description: string;
  done: boolean;
}
```

- $ touch Filter.ts
- editar Filter.ts:

```javascript
export enum Filter {
  all = "all",
  active = "active",
  done = "done"
}
```

- $ ng generate service services/task
- editar app.module.ts:

```javascript
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  ...
  imports: [
    BrowserModule,
    HttpClientModule
  ],
})
export class AppModule { }

```

- editar task.service.ts:

```JS
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import Item from '../Item';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  private api_url = 'http://localhost:5000/all_items';

  constructor(private http: HttpClient) {}

  get_items(): Observable<Item[]> {
    return this.http.get<Item[]>(this.api_url);
  }

  delete_item(item: Item): Observable<Item> {
    const url = `${this.api_url}/${item.id}`;
    return this.http.delete<Item>(url);
  }

  update_item(item: Item): Observable<Item> {
    const url = `${this.api_url}/${item.id}`;
    return this.http.put<Item>(url, item, httpOptions);
  }

  add_item(item: Item): Observable<Item> {
    return this.http.post<Item>(this.api_url, item, httpOptions);
  }
}
```

- editar app.component.ts:

```JS
import { Component, OnInit } from '@angular/core';

import { TaskService } from './services/task.service';
import { Filter } from './Filter';
import Item from './Item';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'json-server TODO app';
  appFilter = Filter;

  current_filter = this.appFilter.all;

  all_tasks: Item[] = [];

  constructor(private taskService: TaskService) {}

  ngOnInit(): void {
    this.get_tasks();
  }

  get_tasks() {
    this.taskService.get_items().subscribe((all_items) => {
      this.all_tasks = all_items;
    });
  }

  get filter_tasks() {
    if (this.current_filter === this.appFilter.all) {
      return this.all_tasks;
    }
    if (this.current_filter === this.appFilter.done) {
      return this.all_tasks.filter((item) => item.done);
    }
    return this.all_tasks.filter((item) => !item.done);
  }

  delete_task(task: Item) {
    this.taskService.delete_item(task).subscribe(() => {
      this.all_tasks = this.all_tasks.filter((t) => t.id !== task.id);
    });
  }

  update_task(task: Item) {
    this.taskService.update_item(task).subscribe(() => {
      this.get_tasks();
    });
  }

  add_task(description: string) {
    if (!description) return;
    let task: Item = { description, done: false };
    this.taskService.add_item(task).subscribe((new_task) => {
      this.all_tasks.push(new_task);
    });
  }
}
```

- crear componente item: $ ng g component components/item
- editar item.component.ts

```JS
import { Component, Input, Output, EventEmitter } from '@angular/core';
import Item from 'src/app/Item';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'],
})
export class ItemComponent {
  @Input() item!: Item;
  @Output() update = new EventEmitter<Item>();
  @Output() remove = new EventEmitter<Item>();
  editable = false;
}
```

- editar item.component.html:

```xml
<div class="row">
  <label [for]="item.description" class="col form-check-label">
    {{ item.description }}
  </label>
  <input
    class="form-check-input"
    [checked]="item.done"
    [id]="item.description"
    type="checkbox"
    (change)="item.done = !item.done; update.emit()"
  />

  <div class="col btn-group" role="group">
    <button class="btn btn-primary" (click)="editable = !editable">Edit</button>
    <button class="btn btn-danger" (click)="remove.emit()">Delete</button>
  </div>

  <div *ngIf="editable" class="row mt-1">
    <input
      #editableItem
      class="col form-control"
      placeholder="Edit item"
      type="text"
      value="{{ item.description }}"
      (keyup.enter)="item.description = editableItem.value; update.emit()"
    />

    <div class="col btn-group">
      <button class="btn btn-danger" (click)="editable = !editable">
        Cancel
      </button>
      <button
        class="btn btn-primary"
        (click)="item.description = editableItem.value; update.emit()"
      >
        Save
      </button>
    </div>
  </div>
</div>
```

- actualizar app.component.html:

```xml
<div class="container">
  <h1>
    {{ title }}
  </h1>

  <hr class="border border-primary border-2 opacity-75" />

  <label for="addItemInput" class="form-label">
    What would you like to do today?
  </label>
  <input
    #newItem
    class="form-control mb-1"
    id="addItemInput"
    placeholder="Add new item"
    type="text"
    (keyup.enter)="add_task(newItem.value); newItem.value = ''"
  />

  <div class="btn-group mb-4">
    <div
      class="btn btn-primary"
      (click)="add_task(newItem.value); newItem.value = ''"
    >
      Add
    </div>

    <button
      class="btn btn-secondary"
      [class.active]="current_filter == appFilter.all"
      (click)="current_filter = appFilter.all"
    >
      View All
    </button>

    <button
      class="btn btn-secondary"
      [class.active]="current_filter == appFilter.active"
      (click)="current_filter = appFilter.active"
    >
      View Incomplete
    </button>

    <button
      class="btn btn-secondary"
      [class.active]="current_filter == appFilter.done"
      (click)="current_filter = appFilter.done"
    >
      View Done
    </button>
  </div>

  <hr class="border border-primary border-2 opacity-75" />

  <h2>
    {{ filter_tasks.length }}
    <span *ngIf="all_tasks.length === 1; else elseBlock"> item </span>
    <ng-template #elseBlock>items</ng-template>
  </h2>

  <!-- <ul class="list-group">
    <li
      class="list-group-item mb-1"
      *ngFor="let item of filter_tasks"
      [ngStyle]="{ 'margin-top': '5px' }"
      ngClass="{{ item.done ? '' : 'list-group-item-success' }}"
    >
      {{ item.description }}
    </li>
  </ul> -->

  <ul class="list-group">
    <li *ngFor="let i of filter_tasks" class="list-group-item mb-1">
      <app-item
        [item]="i"
        (update)="update_task(i)"
        (remove)="delete_task(i)"
      ></app-item>
    </li>
  </ul>
</div>
```

## Publicar en gitlab (pasos similares para github)

- crear cuenta en gitlab (j_sauceda)
- crear repositorio (angular-json-todo)
- instalar git en la máquina local
- ejecutar $ git init --initial-branch=master
- $ git config user.name "Jorge Sauceda"
- $ git config user.email "jorge_sauceda@hotmail.com"
- $ git remote add origin https://gitlab.com/j_sauceda/angular-json-todo.git
- $ git add .
- $ git commit -m "Initial commit"
- $ git status // para confirmar el branch que usamos (master)
- $ git push -u origin master
- navegar en gitlab para comprobar que hemos sincronizado el repositorio
